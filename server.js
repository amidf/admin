const request = require('request-promise')
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiRG1pdHJ5IEZlZGluIiwidGFza3MiOlsibmV3X3Rhc2siLCJzZWNvbmRfdGFzayJdLCJpYXQiOjE1MzI0MzMzNTEsImV4cCI6MTUzMzAzODE1MSwiYXVkIjoic3RhZ2luZy5kYXNoYS5haSIsImlzcyI6ImRhc2hhLmFpIn0._yt4WdPCeyCNulgWSONUluapXAOgJNyA5sJcJEsP_S0'

const fs = require('fs')

const body = JSON.parse(fs.readFileSync('./example.json'))

const options = {
  uri: `https://manager.staging.dasha.ai/api/v1/conversations/`,
  method: 'POST',
  body: { data: body },
  headers: {
    'Authorization': `Bearer ${token}`
  },
  json: true
}

request(options).then(body => console.log(body)).catch(error => console.log(error))
